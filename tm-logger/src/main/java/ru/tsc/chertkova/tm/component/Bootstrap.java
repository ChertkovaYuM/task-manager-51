package ru.tsc.chertkova.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.service.IReceiverService;
import ru.tsc.chertkova.tm.listener.LoggerListener;
import ru.tsc.chertkova.tm.service.ReceiverService;

public final class Bootstrap {

    @SneakyThrows
    public void init() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

}
