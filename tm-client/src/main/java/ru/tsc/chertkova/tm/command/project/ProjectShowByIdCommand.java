package ru.tsc.chertkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.project.ProjectShowByIdRequest;
import ru.tsc.chertkova.tm.dto.response.project.ProjectShowByIdResponse;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectShowByIdResponse response = getServiceLocator().getProjectEndpoint()
                .showProjectById(new ProjectShowByIdRequest(getToken(), id));
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

}
