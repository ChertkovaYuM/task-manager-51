package ru.tsc.chertkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void clear();

    @NotNull
    List<M> findAll();

    @Nullable
    M findById(@NotNull String id);

    int getSize();

    M removeById(@NotNull String id);

    void update(@NotNull M model);

}
