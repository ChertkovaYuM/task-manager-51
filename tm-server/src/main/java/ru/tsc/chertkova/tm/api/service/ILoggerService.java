package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void info(@NotNull String message);

    void debug(@NotNull String message);

    void command(@NotNull String message);

    void error(@NotNull Exception e);

}
