package ru.tsc.chertkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IAbstractUserOwnerModelRepository<Project> {

    void add(@NotNull String userId,
             @NotNull Project model);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull String userId,
                     @NotNull String id);

    int getSize(@NotNull String userId);

    Project removeById(@NotNull String userId,
                       @NotNull String id);

    void update(@NotNull String userId,
                @NotNull Project model);

    void changeStatus(@NotNull String id,
                      @NotNull String userId,
                      @NotNull String status);

    int existsById(@Nullable String id);

}
