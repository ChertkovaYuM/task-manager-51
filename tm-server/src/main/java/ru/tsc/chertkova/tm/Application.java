package ru.tsc.chertkova.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.component.Bootstrap;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
