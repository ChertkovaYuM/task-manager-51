package ru.tsc.chertkova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.User;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable User user) {
        super(user);
    }

}
