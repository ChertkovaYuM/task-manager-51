package ru.tsc.chertkova.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ApplicationAboutResponse extends AbstractResponse {

    @Nullable
    private String email;

    @Nullable
    private String name;

}
